const net = require("net");
const beginMarker = Buffer.from("@@-BEGIN-@@");
const endMarker = Buffer.from("@@-END-@@");
const userMarker = Buffer.from("@@-USER-@@");

let client = null;

function openConnection(options) {
	client = net.createConnection(options.port, options.host, () => {
		console.log("Connected to server");
		const userKeyLength = Buffer.from([options.userKey.length]);
		client.write(userMarker.toString() + userKeyLength + options.userKey);
	});
	
	client.on("data", (data) => {
		let requestContent = "";
		handleData(data, (text, completed) => {
			requestContent += text;
			if (completed) {
				performRequest(options, requestContent);
			}
		});
	});
	
	client.on("end", () => {
		console.log("Disconnected from server");
	});
	
	client.on("close", () => {
		console.log("Client closed");
	});
	
	client.on("error", (err) => {
		console.log("Client errored. " + err);
		client.end();
	});
}

function closeConnection() {
	client.end();
	client = null;
}

function handleData(data, callback) {
	let dataStartOffset = 0;
	let dataEndOffset = 0;
	do {
		let obtainedCompleteTransmission = false;
		
		if (data.slice(0, beginMarker.length).equals(beginMarker)) {
			dataStartOffset = beginMarker.length;
		}
		
		dataEndOffset = data.indexOf(endMarker, dataStartOffset);
		if (dataEndOffset < 0) {
			dataEndOffset = data.length;
		}
		else {
			obtainedCompleteTransmission = true;
		}
		
		if (dataEndOffset > dataStartOffset) {
			const msg = data.slice(dataStartOffset, dataEndOffset);
			callback(msg, obtainedCompleteTransmission);
		}
		
		data = data.slice(dataEndOffset + endMarker.length);
	}
	while (data.length > 0);
}

function performRequest(options, requestContent) {
	console.log(`Request: ${requestContent}`);
	const forwardUrl = new URL(options.forwardUrl);
	
	let forwardClient = net.createConnection(forwardUrl.port, forwardUrl.hostname, () => {
		console.log("Forward client write");
		forwardClient.write(requestContent);
	});
	
	let beginSendCounter = false;
	let time = new Date();
	let shouldSendCount = 0;
	let responseContent = "";
	
	const intervalKey = setInterval(() => {
		if (!beginSendCounter) {
			return;
		}
		
		// send if 200ms has passed since we last received any data
		if (new Date() - time >= 200) {
			console.log("sending");
			clearInterval(intervalKey);
			client.write(beginMarker + responseContent + endMarker);
			responseContent = "";
		}
		
		shouldSendCount += 1;
		console.log("incrementing send counter: " + shouldSendCount);
	}, 100);
	
	forwardClient.on("data", (data) => {
		console.log("Forward client received data");
		console.log(data.length);
		responseContent += data.toString();
		beginSendCounter = true;
		time = new Date();
	});
	
	forwardClient.on("close", (hadError) => {
		console.log("Forward client close");
		console.log(responseContent);
		
		if (client != null && responseContent) {
			client.write(beginMarker + responseContent + endMarker);
		}
		
		if (hadError) {
			console.log("Closed with error. " + hadError);
		}
	});
	
	forwardClient.on("error", (err) => {
		console.log("Forward client error: " + err);
	});
}